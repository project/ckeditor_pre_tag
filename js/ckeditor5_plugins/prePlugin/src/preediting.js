import { Plugin } from 'ckeditor5/src/core';
import Precommand from './precommand';

// For code lisibility and easy name changes.
export const COMMAND_NAME = 'pre';
export const MODEL_ATTR_NAME = 'pre';

export default class Preediting extends Plugin {
  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'PreEditing';
  }

  /**
   * @inheritDoc
   */
  init() {
    const editor = this.editor;
    editor.model.schema.extend('$text', { allowAttributes: MODEL_ATTR_NAME });
    editor.model.schema.setAttributeProperties(MODEL_ATTR_NAME, {
      isFormatting: false,
      copyOnEnter: true,
    });

    editor.conversion.attributeToElement({
      model: MODEL_ATTR_NAME,
      view: 'pre',
    });

    editor.commands.add(COMMAND_NAME, new Precommand(editor));
  }
}
