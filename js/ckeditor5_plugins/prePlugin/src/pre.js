/**
 * Inspired from ckeditor5-basic-styles/subscript.
 */

import PreEditing from './preediting';
import Preui from './preui';
import { Plugin } from 'ckeditor5/src/core';

export default class Pre extends Plugin {
  static get requires() {
    return [PreEditing, Preui];
  }
}
