import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';

import preIcon from '../../../../icons/pre.svg';
import { COMMAND_NAME } from './preediting';

export const BUTTON_NAME = 'pre';

export default class Preui extends Plugin {

  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'PreUI';
  }

  /**
   * @inheritDoc
   */
  init() {
    const editor = this.editor;
    const t = editor.t;

    editor.ui.componentFactory.add(BUTTON_NAME, locale => {
      const command = editor.commands.get(COMMAND_NAME);
      const view = new ButtonView(locale);

      view.set({
        label: t('Pre'),
        icon: preIcon,
        tooltip: true,
        isToggleable: true,
      });

      view.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      this.listenTo(view, 'execute', () => {
        editor.execute(COMMAND_NAME);
        editor.editing.view.focus();
      });

      return view;
    });
  }
}
